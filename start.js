/** eslint-env node, es6 */

const http = require('http');
const fs = require('fs/promises');
const argv = require('minimist')(process.argv.slice(2));

const server = http.createServer(async (request, response) => {
	// let jsCode = '';
	// for (let file of jsFiles) {
	// 	jsCode += await readFile(file);
	// }
	// for (let file of cssFiles) {
	// 	let css = (await readFile(file)).replace(/\s+/g, ' ');
	// 	jsCode += `;mw.loader.addStyleTag('${css}');`;
	// }

	if (request.url === '/') {
		// Entry point
		const files = argv._;
		let jsLoaderCode = files
			.filter(f => f.endsWith('.js'))
			.map(fileName => `mw.loader.load('http://${hostname}:${port}/${fileName}');`)
			.join('\n');
		let cssLoaderCode = files
			.filter(f => f.endsWith('.css'))
			.map(fileName => `mw.loader.load('http://${hostname}:${port}/${fileName}', 'text/css');`)
			.join('\n');
		let loaderCode = jsLoaderCode + cssLoaderCode;
		response.writeHead(200, { 'Content-Type': 'text/javascript; charset=utf-8' });
		response.end(loaderCode, 'utf-8');

	} else {

		// Single-file request
		const filePath = '.' + request.url;
		let contentType = request.url.endsWith('.js')
			? 'text/javascript' : request.url.endsWith('.css')
			? 'text/css': 'text/plain';
		try {
			let content = (await fs.readFile(__dirname + request.url)).toString();
			response.writeHead(200, { 'Content-Type': contentType + '; charset=utf-8' });
			response.end(content, 'utf-8');
		} catch (error) {
			response.end('Oops, something went wrong: ' + error.code + ' ..\n');
		}
	}
});

const hostname = '127.0.0.1';
const port = argv.port || '5500';

server.listen(port, hostname, () => {
	console.log(`Server running at http://${hostname}:${port}/`);
});
