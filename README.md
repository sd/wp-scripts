# wp-scripts

This repository contains the development setup for user script development.

* tsconfig.json: TypeScript configuration. Enables use of TypeScript-based IDE IntelliSense in JS files. 
* package.json: Dependencies includes [types-mediawiki](https://www.npmjs.com/package/types-mediawiki) for the TypeScript types for mediawiki JS interface, and [mock-mediawiki](https://www.npmjs.com/package/mock-mediawiki) for testing code involving mediawiki modules.
* start.js: Development server. The JS and CSS files specified in cli arguments are served over <http://localhost:5500>. Since all scripts are loaded through this same URL, you don't have to change your on-wiki common.js page every time. A single request is made to localhost:5500, the response is some JavaScript which instructs the client to re-request each file individually. This enables you to debug through an IDE. This is similar to how the ResourceLoader's debug=true parameter works during MediaWiki core/extension development.
* serve.run.xml: IntelliJ run configuration to invoke start.js with the file currently open in the editor.
